<?php
include(__DIR__ . '/sglobals.php');
if(!function_exists('error')) {
	function error($msg) {
		global $h;
		echo "<div class='error'><h3>Error</h3>".$msg."</div>";
		exit($h->endpage());
	}
}
if(!function_exists('success')) {
	function success($msg, $fill = false) {
		global $h;
		echo "<div class='success'><h3>Success</h3>".$msg."</div>";
		if($kill)
			exit($h->endpage());
	}
}
if(!function_exists('format')) {
	function format($str, $dec = 0) {
		$ret = is_numeric($str) ? number_format($str, $dec) : stripslashes(strip_tags($str));
		if(!is_numeric($str) && $dec)
			$ret = nl2br($ret);
		return $ret;
	}
}
if(!in_array($ir['user_level'], [2, 3])) // Add/remove user_levels according to access requirements. Default access: administrator, secretary
	error("You don't have access");
$_GET['ID'] = isset($_GET['ID']) && ctype_digit($_GET['ID']) ? $_GET['ID'] : null;
$_GET['action'] = isset($_GET['action']) && ctype_alpha($_GET['action']) ? strtolower(trim($_GET['action'])) : null;
switch($_GET['action']) {
	case 'add':
		addHint();
		break;
	case 'edit':
		editHint();
		break;
	case 'del':
		deleteHint();
		break;
	case 'switch':
		deActivateHint();
		break;
	default:
		viewHints();
		break;
}
function viewHints() {
	global $db;
	$selectHints = $db->query("SELECT * FROM `hints` ORDER BY `id` ASC");
	?><h3>Hints: Index</h3>
	<h4><a href='staff_hints.php?action=add'>Add Hint</a></h4>
	<table class='table' width='75%'>
		<tr>
			<th width='65%'>Hint</th>
			<th width='35%'>Actions</th>
		</tr><?php
		if(!$db->num_rows($selectHints))
			echo "<tr><td colspan='2' class='center'>There are no hints</td></tr>";
		else
			while($row = $db->fetch_row($selectHints)) {
				?><tr>
					<td><?php echo format($row['hint']);?></td>
					<td>[<a href='staff_hints.php?action=edit&amp;ID=<?php echo $row['id'];?>'>Edit</a>] [<a href='staff_hints.php?action=switch&amp;ID=<?php echo $row['id'];?>'><?php echo $row['active'] ? 'Deactivate' : 'Activate';?></a>] [<a href='staff_hints.php?action=del&amp;ID=<?php echo $row['id'];?>'>Delete</a>]</td>
				</tr><?php
			}
	?></table><?php
}
function addHint() {
	global $db;
	?><h3>Hints: Add</h3><?php
	if(array_key_exists('submit', $_POST)) {
		$_POST['hint'] = isset($_POST['hint']) && is_string($_POST['hint']) ? $db->escape($_POST['hint']) : null;
		if(empty($_POST['hint']))
			error("You didn't enter a valid hint");
		$selectDup = $db->query("SELECT `id` FROM `hints` WHERE `hint` = '".$_POST['hint']."'");
		if($db->num_rows($selectDup))
			error("That hint has already been added");
		$_POST['active'] = isset($_POST['active']) && ctype_digit($_POST['active']) && in_array($_POST['active'], array(0, 1)) ? $_POST['active'] : null;
		if(empty($_POST['active']))
			error("You didn't select a valid active status");
		$db->query("INSERT INTO `hints` ( `hint`, `active`) VALUES ('".$_POST['hint']."', ".$_POST['active'].")");
		stafflog_add("Added a new hint");
		success("Your new hint has been added");
		viewHints();
	}
	?><form action='staff_hints.php?action=add' method='post'>
		<table class='table' width='75%'>
			<tr>
				<th width='25%'>Hint</th>
				<td width='75%'><input type='text' name='hint' size='98%' /></td>
			</tr>
			<tr>
				<th>Active</th>
				<td>
					<input type='radio' name='active' value='1' selected='selected' />Yes<br />
					<input type='radio' name='active' value='0' />No
				</td>
			</tr>
			<tr>
				<td colspan='2' class='center'><input type='submit' name='submit' value='Add Hint' /></td>
			</tr>
		</table>
	</form><?php
}
function editHint() {
	global $db;
	?><h3>Hints: Edit</h3><?php
	if(empty($_GET['ID']))
		error("You didn't select a valid hint");
	$select = $db->query("SELECT `hint`, `active` FROM `hints` WHERE `id` = ".$_GET['ID']);
	if(!$db->num_rows($select))
		error("That hint doesn't exist");
	$row = $db->fetch_row($select);
	if(array_key_exists('submit', $_POST)) {
		$_POST['hint'] = isset($_POST['hint']) && is_string($_POST['hint']) ? $db->escape($_POST['hint']) : null;
		if(empty($_POST['hint']))
			error("You didn't enter a valid hint");
		$_POST['active'] = isset($_POST['active']) && ctype_digit($_POST['active']) && in_array($_POST['active'], array(0, 1)) ? $_POST['active'] : null;
		if(empty($_POST['active']))
			error("You didn't select a valid active status");
		$selectDup = $db->query("SELECT `id` FROM `hints` WHERE `hint` = '".$_POST['hint']."' AND `id` <> ".$_GET['ID']);
		if($db->num_rows($selectDup))
			error("That hint has already been added");
		$db->query("UPDATE `hints` SET `hint` = '".$_POST['hint']."', `active` = ".$_POST['active']." WHERE `id` = ".$_GET['ID']);
		stafflog_add("Edited a hint");
		success("Your hint has been edited");
		viewHints();
	} else {
		?><form action='staff_hints.php?action=edit&amp;ID=<?php echo $_GET['ID'];?>' method='post'>
			<table class='table' width='75%'>
				<tr>
					<th width='25%'>Hint</th>
					<td width='75%'><input type='text' name='hint' size='98%' value='<?php echo $mtg->format($row['hint']);?>' /></td>
				</tr>
				<tr>
					<th>Active</th>
					<td>
						<input type='radio' name='active' value='1'<?php echo $row['active'] ? " selected='selected'" : '';?> />Yes<br />
						<input type='radio' name='active' value='0'<?php echo !$row['active'] ? " selected='selected'" : '';?> />No
					</td>
				</tr>
				<tr>
					<td colspan='2' class='center'><input type='submit' name='submit' value='Add Hint' /></td>
				</tr>
			</table>
		</form><?php
	}
}
function deleteHint() {
	global $db;
	if(empty($_GET['ID']))
		error("You didn't select a valid hint");
	$select = $db->query("SELECT `hint` FROM `hints` WHERE `id` = ".$_GET['ID']);
	if(!$db->num_rows($select))
		error("That hint doesn't exist");
	$hint = $db->fetch_single($select);
	if(array_key_exists('ans', $_GET)) {
		$db->query("DELETE FROM `hints` WHERE `id` = ".$_GET['ID']);
		stafflog_add("Deleted a hint");
		success("Your hint has been deleted");
		viewHints();
	} else {
		?>Selected hint: <?php echo $mtg->format($hint);?><br />
		Are you sure you wish to delete this hint?<br />
		<a href='staff_hints.php?action=del&amp;ID=<?php echo $_GET['ID'];?>&amp;ans=yes'>Yes, delete the hint</a> &middot; <a href='staff_hints.php'>No, take me back</a><?php
	}
}
function deActivateHint() {
	global $db;
	if(empty($_GET['ID']))
		error("You didn't select a valid hint");
	$select = $db->query("SELECT `hint`, `active` FROM `hints` WHERE `id` = ".$_GET['ID']);
	if(!$db->num_rows($select))
		error("That hint doesn't exist");
	$row = $db->fetch_row($select);
	$new = $row['active'] ? 'Dea' : 'A';
	$db->query("UPDATE `hints` SET `active` = IF(`active` = 1, 0, 1) WHERE `id` = ".$_GET['ID']);
	stafflog_add($new."ctivated a hint");
	success("You've ".strtolower($new)."ctivated the hint");
	viewHints();
}
$h->endpage();