# This has been tested for __parse errors only__.
### I no longer use the MC Craps engine so I have no place to test (nor do I have a copy of MC Craps..)

# Hints

### Database updates
Import and run staff_hints.sql

### staff.php - update to the basic settings
Find something like `<input type='submit' value='Submit' />` in the form of the basic settings function.
Add above:
```PHP
";
?><br /><br />
<strong>Maximum Level for Hints to Display:</strong> <input type='text' name='hint_max_level' value='<?php echo $set['hint_max_level']; ?>' /><br />
```
### staff_hints.php
Upload staff_hints.php

### smenu.php - edit
Add wherever you want the link to display
`<a href='staff_hints.php'>Manage Hints</a>`

### header.php - edit
Add wherever you want the hint to display (thank you to Guest for the caching system - I've minorly updated it)
```PHP
if($set['hint_max_level'] >= $ir['level']) {
	if(isset($_SESSION['hints_header']) && count($_SESSION['hints_header'])) {
		$hint = mt_rand(0, count($_SESSION['hints_header']) - 1);
		echo $_SESSION['hints_header'][$hint];
	} else {
		$selectHints = $db->query("SELECT `hint` FROM `hints` WHERE `active` = 1 ORDER BY `id` ASC");
		if($db->num_rows($selectHints)) {
			$cnt = 0;
			while($row = $db->fetch_row($select)) {
				$_SESSION['hints_header'][$cnt] = $row['hint'];
				++$cnt;
			}
			echo $_SESSION['hints_header'][0];
		}
	}
}
```

### Optional images
Note: If you want these icons, you'll need to create a directory path of `/public_html/images` then upload them

http://ezisite.com.au/masonry/widgets/simple/skindefault/admin/q.gif

##### Note
Setting the max hint level to 0 will effectively disable the hints