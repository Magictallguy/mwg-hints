CREATE TABLE `hints` (
	`id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
	`hint` VARCHAR(255) NOT NULL DEFAULT '',
	`active` TINYINT(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `settings` (`conf_name`, `conf_value`) VALUES ('hint_max_level', 5);